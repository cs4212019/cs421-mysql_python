# Database scripts
The CS421-GUI script was primarily written by Hiram Temple, with contributions from Nicholas Grogg <br>
The CS421-CLI script was written by Nicholas Grogg <br>
Created Spring 2019 for CS421 at the University of Hawaii at Hilo <br>

Scripts can be modified as needed for your purposes. <br>

## PLANNED EXPANSIONS
A Join mechanic similar to the Where line in the Query sections <br>

## ISSUES
Iterator needed for Export file names in GUI <br>
Formatting could be more visually appealing <br>
